;; bsynq/gnucash/reader.scm --- reader implementation -*- coding: utf-8 -*-
;;
;; Copyright (C) 2018 Alex Sassmannshausen <alex@pompo.co>
;;
;; Author: Alex Sassmannshausen <alex@pompo.co>
;;
;; This file is part of guile-bsynq.
;;
;; guile-bsynq is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the Free
;; Software Foundation; either version 3 of the License, or (at your option)
;; any later version.
;;
;; guile-bsynq is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with guile-bsynq; if not, contact:
;;
;; Free Software Foundation           Voice:  +1-617-542-5942
;; 59 Temple Place - Suite 330        Fax:    +1-617-542-2652
;; Boston, MA  02111-1307,  USA       gnu@gnu.org

;;; Commentary:
;;
;;; Code:

(define-module (bsynq gnucash reader)
  #:use-module (ice-9 format)
  #:use-module (ice-9 hash-table)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (sxml simple)
  #:use-module (sxml xpath)
  #:use-module (sxml match)
  #:use-module ((bsynq types) #:prefix types:)
  #:export (gnucash->sxml
            gnucash->bsynq

            gnucash->commodity
            gnucash->price
            gnucash->transaction
            gnucash->account
            gnucash->split

            gnucash-commodities
            gnucash-accounts
            gnucash-prices
            gnucash-transactions))

(define* (gnucash->bsynq filename #:key (currency #f))
  "Return a function that, when passed an sxml-query attempts to reify the
nodeset resulting from the attempted reification of the xml at STR/PORT.

Objects will be reified into bsynq objects if possible."
  (lambda (sxml-query)
    "Return the result of attempting to reify the result of SXML-QUERY against
the interned sxml into bsynq objects."
    (map (lambda (nodeset)
           (match nodeset
             (('gnc:commodity . rest)
              (gnucash->commodity nodeset))
             (('price . rest)
              (gnucash->price nodeset))
             (('gnc:pricedb . rest)
              (map gnucash->price (cddr nodeset)))
             (('gnc:account . rest)
              (gnucash->account nodeset))
             (('gnc:transaction . rest)
              (gnucash->transaction nodeset))
             (('trn:splits . splits)
              (map (cut gnucash->split <> currency) splits))
             (('trn:split . rest)
              (gnucash->split nodeset currency))
             (unknown_result unknown_result)))
         (with-input-from-file filename
           (lambda _
             (sxml-query (gnucash->sxml)))))))

(define gnucash->sxml
  (let ((db #f))
    (lambda* (#:optional (str/port (current-input-port)) force?)
      (when (or force? (not db))
        (set! db (xml->sxml str/port #:trim-whitespace? #t
                            #:namespaces
                            '((gnc . "http://www.gnucash.org/XML/gnc")
                              (act . "http://www.gnucash.org/XML/act")
                              (book . "http://www.gnucash.org/XML/book")
                              (cd . "http://www.gnucash.org/XML/cd")
                              (cmdty . "http://www.gnucash.org/XML/cmdty")
                              (price . "http://www.gnucash.org/XML/price")
                              (slot . "http://www.gnucash.org/XML/slot")
                              (split . "http://www.gnucash.org/XML/split")
                              (sx . "http://www.gnucash.org/XML/sx")
                              (trn . "http://www.gnucash.org/XML/trn")
                              (ts . "http://www.gnucash.org/XML/ts")
                              (fs . "http://www.gnucash.org/XML/fs")
                              (bgt . "http://www.gnucash.org/XML/bgt")
                              (recurrence . "http://www.gnucash.org/XML/recurrence")
                              (lot . "http://www.gnucash.org/XML/lot")
                              (addr . "http://www.gnucash.org/XML/addr")
                              (owner . "http://www.gnucash.org/XML/owner")
                              (billterm . "http://www.gnucash.org/XML/billterm")
                              (bt-days . "http://www.gnucash.org/XML/bt-days")
                              (bt-prox . "http://www.gnucash.org/XML/bt-prox")
                              (cust . "http://www.gnucash.org/XML/cust")
                              (employee . "http://www.gnucash.org/XML/employee")
                              (entry . "http://www.gnucash.org/XML/entry")
                              (invoice . "http://www.gnucash.org/XML/invoice")
                              (job . "http://www.gnucash.org/XML/job")
                              (order . "http://www.gnucash.org/XML/order")
                              (taxtable . "http://www.gnucash.org/XML/taxtable")
                              (tte . "http://www.gnucash.org/XML/tte")
                              (vendor
                               . "http://www.gnucash.org/XML/vendor")))))
      db)))

;;;; Commodities

(define gnucash-commodities
  (sxpath `(gnc-v2 gnc:book gnc:commodity)))

(define commodities-db
  (let ((cache #f))
    (lambda ()
      (when (not cache)
        (let ((commodities (gnucash-commodities (gnucash->sxml))))
          (set! cache
                (alist->hash-table
                 (map cons
                      (map cons
                           (commodity-id commodities)
                           (commodity-space commodities))
                      commodities)))))
      cache)))

(define (gnucash->commodity nodeset)
  (match nodeset
    (('gnc:commodity _ (_ space) (_ id) (or (_ get_quotes) (_ . get_quotes))
                     (_ quote_source) (or (_ quote_tz) (_ . quote_tz)))
     (match (types:resolve-commodity types:commodity-id id)
       ((? types:commodity? c) c)
       (((? types:commodity? c) ...)
        (throw 'gnucash->commodity "Multiple commodities found:" c))
       (#f (types:commodity id "" "" space get_quotes quote_source quote_tz))))))

;;;;; Accessors

(define commodity-id
  (sxpath `(cmdty:id *text*)))

(define commodity-space
  (sxpath `(cmdty:space *text*)))

(define commodity-get_quotes
  (sxpath `(cmdty:get_quotes)))

(define commodity-quote_source
  (sxpath `(cmdty:quote_source *text*)))

(define commodity-quote_tz
  (sxpath `(cmdty:quote_tz)))

;;;; Prices

(define gnucash-prices
  (sxpath `(gnc-v2 gnc:book gnc:pricedb price)))

(define (gnucash->price nodeset)
  (define (commodity xsor)
    (lambda (n)
      (match (xsor n)
        (((_ (_ space) (_ id)))
         (match (types:resolve-commodity types:commodity-id id)
           ((? types:commodity? c) c)
           (((? types:commodity? c) ...)
            (throw 'gnucash->price "Multiple commodities found:" c))
           (#f (types:commodity id "" "" space '() #f #f))))
        (e (throw 'gnucash-price "Unexpected commodity format" e)))))
  (apply types:price
         (map (lambda (p)
                (match (p nodeset)
                  (() #f)
                  ((n) n)
                  (n n)))
              (list price-id (commodity price-commodity)
                    (commodity price-currency) price-time price-source
                    price-type price-value))))

;;;; Accessors

(define price-id
  (sxpath `(price:id *text*)))

(define price-commodity
  (sxpath `(price:commodity)))

(define price-currency
  (sxpath `(price:currency)))

(define price-time
  (sxpath `(price:time ts:date *text*)))

(define price-source
  (sxpath `(price:source *text*)))

(define price-type
  (sxpath `(price:type *text*)))

(define price-value
  (compose list
           string->number
           first
           (sxpath `(price:value *text*))))

;;;; Accounts

(define gnucash-accounts
  (sxpath `(gnc-v2 gnc:book gnc:account)))

(define accounts-db
  (let ((cache #f))
    (lambda ()
      (when (not cache)
        (let ((accounts (gnucash-accounts (gnucash->sxml))))
          (set! cache
                (alist->hash-table
                 (map cons (account-id accounts) accounts)))))
      cache)))

(define (gnucash->account nodeset)
  (define (commodity xsor)
    (lambda (n)
      (match (xsor n)
        (() #f)
        (((_ (_ space) (_ id)))
         (match (types:resolve-commodity types:commodity-id id)
           ((? types:commodity? c) c)
           (((? types:commodity? c) ...)
            (throw 'gnucash->accounts "Multiple commodities found:" c))
           (#f (types:commodity id "" "" space '() #f #f))))
        (e (throw 'gnucash-accounts "Unexpected commodity format" e)))))
  (apply types:account
         (map (lambda (p)
                (match (p nodeset)
                  (() #f)
                  ((n) n)
                  (n n)))
              (list account-id account-full-name (commodity account-commodity)
                    account-description))))

;;;;; Accessors

(define (account-full-name accounts)
  (map (lambda (account)
         (let lp ((path '())
                  (account account))
           (match (account-parent account)
             ;; The only account with no parent is Root Account, and we don't
             ;; need it in the full-name, so we just join path now.
             (() (string-join path ":"))
             ((parent-guid) (lp (cons (first (account-name account)) path)
                                (hash-ref (accounts-db) parent-guid))))))
       (or (and (not (null? accounts)) (eq? 'gnc:account (first accounts))
                (list accounts))
           accounts)))

(define account-id
  (sxpath `(act:id *text*)))

(define account-name
  (sxpath `(act:name *text*)))

(define account-commodity
  (sxpath `(act:commodity)))

(define account-commodity-currency
  (sxpath `(act:commodity cmdty:id *text*)))

(define account-description
  (sxpath `(act:description *text*)))

(define account-parent
  (sxpath `(act:parent *text*)))

;;;; Transactions

(define gnucash-transactions
  (sxpath `(gnc-v2 gnc:book gnc:transaction)))

(define* (gnucash->transaction nodeset)
  (define (commodity)
    (match (transaction-commodity nodeset)
      (() #f)
      (((_ (_ space) (_ id)))
       (match (types:resolve-commodity types:commodity-id id)
         ((? types:commodity? c) c)
         (((? types:commodity? c) ...)
          (throw 'gnucash->transaction "Multiple commodities found:" c))
         (#f (types:commodity id "" "" space '() #f #f))))
      (e (throw 'gnucash-transaction "Unexpected commodity format" e))))
  (define value
    (match-lambda
      (() #f)
      ((n) n)
      (n n)))
  (let* ((currency (commodity))
         (splits (map (cut gnucash->split <> currency)
                      (transaction-splits nodeset))))
    (types:transaction (value (transaction-id nodeset))
                       (value (transaction-date-posted nodeset))
                       (every types:split-cleared splits)
                       (value (transaction-number nodeset))
                       (value (transaction-description nodeset))
                       currency
                       splits)))

;;;;; Accessors

(define transaction-id
  (sxpath `(trn:id *text*)))

(define transaction-number
  (sxpath `(trn:num *text*)))

(define transaction-commodity
  (sxpath `(trn:currency)))

(define transaction-commodity-currency
  (sxpath `(trn:currency cmdty:id *text*)))

(define transaction-description
  (sxpath `(trn:description *text*)))

(define transaction-date-posted
  (sxpath `(trn:date-posted ts:date *text*)))

(define transaction-date-entered
  (sxpath `(trn:date-entered ts:date *text*)))

;;;; Splits

(define transaction-splits
  (sxpath `(trn:splits trn:split)))

(define* (gnucash->split nodeset #:optional currency)
  (let ((account (gnucash->account
                  (hash-ref (accounts-db)
                            (first (split-account nodeset))))))
    (apply types:split
           (map (lambda (p)
                  (match (p nodeset)
                    (() #f)
                    ((n) n)
                    (n n)))
                (list split-id
                      (compose (cut string=? "y" <>) first
                               split-reconciled_state)
                      (const account)
                      (const (types:account-commodity account))
                      split-value split-quantity)))))

;;;;; Accessors

(define split-id
  (sxpath `(split:id *text*)))

(define split-reconciled_state
  (sxpath `(split:reconciled-state *text*)))

(define split-account
  (sxpath `(split:account *text*)))

(define split-reconcile_date
  (sxpath `(ts:date *text*)))

(define split-value
  (compose list string->number first (sxpath `(split:value *text*))))

(define split-quantity
  (compose list string->number first (sxpath `(split:quantity *text*))))
