;; bsynq/ledger/writer.scm --- writer implementation    -*- coding: utf-8 -*-
;;
;; Copyright (C) 2018 Alex Sassmannshausen <alex@pompo.co>
;;
;; Author: Alex Sassmannshausen <alex@pompo.co>
;;
;; This file is part of guile-bsynq.
;;
;; guile-bsynq is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the Free
;; Software Foundation; either version 3 of the License, or (at your option)
;; any later version.
;;
;; guile-bsynq is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with guile-bsynq; if not, contact:
;;
;; Free Software Foundation           Voice:  +1-617-542-5942
;; 59 Temple Place - Suite 330        Fax:    +1-617-542-2652
;; Boston, MA  02111-1307,  USA       gnu@gnu.org

;;; Commentary:
;;
;;; Code:

(define-module (bsynq ledger writer)
  #:use-module (bsynq types)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:export (ledger-print))

(define (ledger-print objs)
  (match objs
    (((? transaction?) ...)
     (ledger-print-transactions objs))
    (((? price?) ...)
     (ledger-print-prices objs))))

(define (ledger-print-transactions transactions)
  (for-each (match-lambda
              (($ <transaction> id date cleared code payee currency splits)
               (format #t "~a (~a) ~a"
                       (first (string-split date #\space)) code
                       (string-join (string-split (or payee "") #\newline)
                                    "//"))
               (for-each
                (match-lambda 
                  (($ <split> id cleared account currency value quantity)
                   (format #t "~%  ~60a  "
                           (string-append
                            (if cleared "!" "")
                            (account-name account)))
                   (match (commodity-symbol currency)
                     (#f (format #t "~a ~10,2f"
                                 (commodity-id currency)
                                 (exact->inexact quantity)))
                     (s (format #t "~10,2f~a" (exact->inexact quantity) s)))))
                splits)
               (newline)
               (newline)))
            (sort transactions
                  (lambda trns
                    (apply string<? (map transaction-date trns))))))

(define (ledger-print-prices prices)
  (for-each (lambda (price)
              (format #t "P ~a ~a ~a~10,9f~%"
                      (match (string-split (price-time price) #\space)
                        ((date time zone) (string-append date " " time)))
                      (commodity-symbol (price-commodity price))
                      (commodity-symbol (price-currency price))
                      (exact->inexact (price-value price))))
            (sort prices (lambda prcs
                           (apply string<? (map price-time prcs))))))
