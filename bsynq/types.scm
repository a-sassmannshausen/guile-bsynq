;; bsynq/types.scm --- types implementation    -*- coding: utf-8 -*-
;;
;; Copyright (C) 2018 Alex Sassmannshausen <alex@pompo.co>
;;
;; Author: Alex Sassmannshausen <alex@pompo.co>
;;
;; This file is part of guile-bsynq.
;;
;; guile-bsynq is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the Free
;; Software Foundation; either version 3 of the License, or (at your option)
;; any later version.
;;
;; guile-bsynq is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with guile-bsynq; if not, contact:
;;
;; Free Software Foundation           Voice:  +1-617-542-5942
;; 59 Temple Place - Suite 330        Fax:    +1-617-542-2652
;; Boston, MA  02111-1307,  USA       gnu@gnu.org

;;; Commentary:
;;
;;; Code:

(define-module (bsynq types)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 pretty-print)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-26)
  #:export (<price>
            price price? price-id price-commodity price-currency price-time
            price-source price-type price-value

            <transaction>
            transaction transaction? transaction-date transaction-cleared
            transaction-code transaction-payee transaction-id
            transaction-splits

            <split>
            split split? split-id split-cleared split-account split-currency
            split-value

            <account>
            account account? account-id account-name account-commodity
            account-description account-parent

            <commodity>
            commodity commodity? commodity-id commodity-symbol commodity-name
            commodity-space commodity-get_quotes commodity-quote_source
            commodity-quote_tz

            resolve-commodity))

(define-record-type <price>
  (price id commodity currency time source type value)
  price?
  (id price-id)
  (commodity price-commodity)
  (currency price-currency)
  (time price-time)
  (source price-source)
  (type price-type)
  (value price-value))

(define-record-type <transaction>
  (transaction id date cleared code payee currency splits)
  transaction?
  (id transaction-id)
  (date transaction-date)
  (cleared transaction-cleared)
  (code transaction-code)
  (payee transaction-payee)
  (currency transaction-currency)
  (splits transaction-splits))

(define-record-type <split>
  (split id cleared account currency value quantity)
  split?
  (id split-id)
  (cleared split-cleared)
  (account split-account)
  (currency split-currency)
  (value split-value)
  (quantity split-quantity))

(define-record-type <account>
  (account id name commodity description)
  account?
  (id account-id)
  (name account-name)
  (commodity account-commodity)
  (description account-description))

(define-record-type <commodity>
  (commodity id symbol name space get_quotes quote_source quote_tz)
  commodity?
  (id commodity-id)
  (symbol commodity-symbol)
  (name commodity-name)
  (space commodity-space)
  (get_quotes commodity-get_quotes)
  (quote_source commodity-quote_source)
  (quote_tz commodity-quote_tz))

(define %known_commodities
  (list (commodity "EUR" "€" "Euros" "ISO4217" #f #f #f)
        (commodity "GBP" "£" "Pound Sterling" "ISO4217" #f #f #f)))

(define (resolve-commodity accessor value)
  "Return a list of all known commodities that match the VALUE of criterion
ACCESSOR."
  (match (filter (compose (cut equal? value <>) (cut accessor <>))
                 %known_commodities)
    (() #f)
    ((c) c)
    ((c ...) c)))
