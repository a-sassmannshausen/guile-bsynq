(use-modules
  (guix packages)
  (guix licenses)
  (guix download)
  (guix build-system gnu)
  (gnu packages)
  (gnu packages autotools)
  (gnu packages guile)
  (gnu packages pkg-config)
  (gnu packages texinfo))

(package
  (name "guile-bsynq")
  (version "0.1")
  (source "./guile-bsynq-0.1.tar.gz")
  (build-system gnu-build-system)
  (arguments `())
  (native-inputs
    `(("autoconf" ,autoconf)
      ("automake" ,automake)
      ("pkg-config" ,pkg-config)
      ("texinfo" ,texinfo)))
  (inputs `(("guile" ,guile-2.2)))
  (propagated-inputs `())
  (synopsis "A gnucash reader & ledger writer")
  (description
    "Guile Bsynq is a library to read/write gnucash xml files and ledger files.  Though I primarily wrote it to convert my gnucash datafile to a new ledger data file, so gnucash reading and ledger writing are the only parts that are currently implemented.")
  (home-page
    "https://gitlab.com/a-sassmannshausen/guile-bsynq")
  (license gpl3+))

