(hall-description
  (name "bsynq")
  (prefix "guile")
  (version "0.1")
  (author "Alex Sassmannshausen")
  (copyright (2018))
  (synopsis "A gnucash reader & ledger writer")
  (description
    "Guile Bsynq is a library to read/write gnucash xml files and ledger files.  Though I primarily wrote it to convert my gnucash datafile to a new ledger data file, so gnucash reading and ledger writing are the only parts that are currently implemented.")
  (home-page
    "https://gitlab.com/a-sassmannshausen/guile-bsynq")
  (license gpl3+)
  (dependencies `())
  (files (libraries
           ((scheme-file "bsynq")
            (directory
              "bsynq"
              ((directory "ledger" ((scheme-file "writer")))
               (directory "gnucash" ((scheme-file "reader")))
               (scheme-file "types")))))
         (tests ((directory "tests" ())))
         (programs ((directory "scripts" ())))
         (documentation
           ((text-file "README")
            (text-file "HACKING")
            (text-file "COPYING")
            (directory "doc" ((texi-file "bsynq")))))
         (infrastructure
           ((scheme-file "guix") (scheme-file "hall")))))
